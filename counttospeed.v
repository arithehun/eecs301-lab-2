module counttospeed (
	input clk,
	input enable,
	input[7:0] count_in,  //from u/d counter
	input dir,
	output signed [7:0] speed
);

	
	reg [7:0] init_count = 0;  //same bit width as u/d counter
	reg [7:0] end_count = 0;

	reg signed[7:0] spd; //total guess; max is 2^11
	assign speed = spd;
	
	reg state;
	parameter first = 0, second = 1;
	
	always @(posedge clk) begin
		if(enable) begin
			if(state == first) begin
				init_count <= count_in;
				state = second;
			end
			else if (state == second) begin
				end_count <= count_in;
				if(end_count >= init_count)
					spd <= end_count - init_count;
				else
					spd <= -(init_count - end_count);
				state = first;
			end
			
		end
	end
	
	
	
endmodule