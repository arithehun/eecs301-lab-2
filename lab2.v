module lab2
       (
           //////////////////////// Clock Input ////////////////////////
           input CLOCK_50,
           input CLOCK_50_2,
           //////////////////////// Push Button ////////////////////////
           input [ 2: 0 ] BUTTON,
           //////////////////////// DPDT Switch ////////////////////////
           input [ 9: 0 ] SW,
           //////////////////////// 7-SEG Display ////////////////////////
           output [ 6: 0 ] HEX0_D,
           output HEX0_DP,
           output [ 6: 0 ] HEX1_D,
           output HEX1_DP,
           output [ 6: 0 ] HEX2_D,
           output HEX2_DP,
           output [ 6: 0 ] HEX3_D,
           output HEX3_DP,
           //////////////////////// LED ////////////////////////
           output [ 9: 0 ] LEDG,
           //////////////////////// GPIO ////////////////////////
           input [ 1: 0 ] GPIO0_CLKIN,
           output [ 1: 0 ] GPIO0_CLKOUT,
           inout [ 31: 0 ] GPIO0_D,
           input [ 1: 0 ] GPIO1_CLKIN,
           output [ 1: 0 ] GPIO1_CLKOUT,
           inout [ 31: 0 ] GPIO1_D
       );
	
	wire signed [7:0] spd;
	wire dir;
	
	// Instantiate the quadrature module
	// GPIO 4 and 5 are quad channels A and B respectively
	quadtospeed qspeed(CLOCK_50,GPIO0_D[4],GPIO0_D[5],spd, dir);
	
	// Instantiate the corresponding pwm module
	// Buttons 0, 1, and 2 are increase, decrease, and reset respectively, switch 0 is motor enable, 
	// switches 2-9 are gain constants, and GPIO 0 and 1 are motor power and ground respectively
	pwm motor_driver(CLOCK_50, ~BUTTON[0], ~BUTTON[1], ~BUTTON[2], ~SW[0], spd, SW[9:2], GPIO0_D[0], GPIO0_D[1]);
	
	// Assign switch 0 as the motor enable
	assign GPIO0_D[2] = SW[0];
	
	//====================
	// All unused inout port turn to tri-state
	assign GPIO0_D = 32'hzzzzzzzz;
	assign GPIO1_D = 32'hzzzzzzzz;

endmodule
