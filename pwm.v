module pwm
	(
		input CLOCK_50,
		input SPEED_UP,
		input SPEED_DOWN,
		input SPEED_RESET,
		input MOTOR_ENABLE,
		input SPEED,
		input[7:0] GAIN_CONTROLLER_INPUT,
		output MOTOR_FEED,
		output ANTI_MOTOR_FEED
	);

	//-------------Button assignments--------------//
	reg MOTOR_SIGNAL;
	parameter SPEED_ZERO = 128;
	
	reg signed [7:0] GOAL_SPEED; 
		// Tracks whether the duty cycle should be asserted
	reg signed [8:0] DUTY_TRACKER;  //CHANGED made signed and added a bit

	assign ANTI_MOTOR_FEED = ~MOTOR_SIGNAL;
	assign MOTOR_FEED = MOTOR_SIGNAL;
	
	
	reg CLK_ENABLE = 0;
	reg [1:0] CLK_ENABLE_COUNTER = 0;
	
	// Set a period of 50 MHZ / 8
	always @(posedge CLOCK_50) begin
		if(CLK_ENABLE_COUNTER == 3) begin 
			CLK_ENABLE_COUNTER = 0;
			CLK_ENABLE = ~CLK_ENABLE;
		end
		else CLK_ENABLE_COUNTER = CLK_ENABLE_COUNTER + 1;
		
	end
	
	initial begin
		GOAL_SPEED <= 0;
		MOTOR_SIGNAL <= 0;
	end

	// Increment or decrement the speed when buttons 0 or 1 are pressed
	// made logic synchronous
	parameter unpressed = 0, pressedUp = 1, pressedDown = 2;
	reg [1:0] state = unpressed;
	
	// Made main logic synchronous by transforming into a state machine
	// Added checks to ensure full speed doesn't exceed minimum and maximum values
	// to prevent overflow
	always @(posedge CLOCK_50) begin
	// Change the goal speed
		if(SPEED_UP && state == unpressed) begin
			if(GOAL_SPEED < 126) GOAL_SPEED <= GOAL_SPEED + 1;
			state = pressedUp;
		end
		else if (SPEED_DOWN && state == unpressed) begin
			if(GOAL_SPEED > -126) GOAL_SPEED <= GOAL_SPEED - 1;
			state = pressedDown;
		end
		// Set the goal speed back to 0
		else if (SPEED_RESET) GOAL_SPEED = 0;
		else if(~SPEED_UP && state == pressedUp)
			state = unpressed;
		else if (~SPEED_DOWN && state == pressedDown)
			state = unpressed;
	end
	
	reg signed [20:0] R_VALUE;
	reg signed [7:0] R_PRIME_tmp;
	reg signed [7:0] R_PRIME;
	
	always @(posedge CLOCK_50) begin
		R_VALUE <=  {1'b0, GAIN_CONTROLLER_INPUT } * (GOAL_SPEED - SPEED);	
		R_PRIME_tmp <= R_VALUE >> 8; //gain is btwn 0 and 1, using values from 0 to 255, so shift. note that R_VAL is bigger to allow this
		if(R_PRIME_tmp < 124 && R_PRIME_tmp > -124)
			R_PRIME <= R_PRIME_tmp;
	end
	
	assign LEDG = GOAL_SPEED;
	
	
	always @(posedge CLOCK_50 && CLK_ENABLE) begin
		if(DUTY_TRACKER < (R_PRIME + SPEED_ZERO)) MOTOR_SIGNAL = 1;

		else if(DUTY_TRACKER == 255) DUTY_TRACKER = 0;

		else MOTOR_SIGNAL = 0;
		
		DUTY_TRACKER <= DUTY_TRACKER + 1;
	end


endmodule