module quadtospeed(
	input clk,
	input chA,
	input chB,
	output signed [7:0] spd,
	output direction //1 is forward, 0 backward
);

	wire A, B;
	assign A = chA;
	assign B = chB;
	
	wire en;
	wire upordown;
	wire s_reset;
	wire[7:0] count_out;
	
	updowncounter udc(clk,ud_enable,upordown,s_reset,count_out); //changed clock for testing
	
	
	
	wire overflow;
	assign s_reset = overflow;
	
	overflowcounter ocounter(clk,overflow);
	
	wire cts_en;
	
//	wire signed [7:0] spd;
	assign cts_en = overflow;
	
	counttospeed cts(clk,cts_en,count_out,dir,spd);
	
	reg last_A;
	reg ud_enable;
	reg dir = 1; //1 for forward, 0 backward
	//assign ud_en = ud_enable;
	assign upordown = dir;
	assign direction = dir;
	////
	initial begin
		last_A = A;
	end
		
	always @(posedge clk) begin
		ud_enable = A & ~last_A;     //logic from diagram
		last_A = A;
		dir = B;
	end
	
	
endmodule
















	
