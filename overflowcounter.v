module overflowcounter( 
	input clk,
	output overflow
);

reg [16:0] counter; // 18 bit counter should give 190 hz. this is also a guess.
reg ovflw = 0;
assign overflow = ovflw;

always @(posedge clk) begin
	if(counter == 2**16 - 1)
		ovflw = 1;
	else 
		ovflw = 0;
	counter = counter + 1;
end 

endmodule 