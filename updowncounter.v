module updowncounter (
	input clk,
	input enable,
	input upordown,
	input synchronous_reset,
	output[7:0] numout
);

	wire en, rst,ud;
	assign en = enable;
	assign rst = synchronous_reset;
	assign ud = upordown;
	
	reg [7:0] counter;   //8 bits at a guess
	assign numout = counter;

	always @(posedge clk)
	if(en) begin
		if (rst) begin
			counter <= 0;
		end
		else if (ud) begin
			counter <= counter + 1;
		end 
		else begin
			counter <= counter - 1;
		end
	end    // and pray that it doesnt overflow

endmodule
